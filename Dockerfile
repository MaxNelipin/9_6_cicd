FROM centos:7

RUN mkdir /python_api
COPY python-api.py /python_api/python-api.py
COPY requiremets.txt /python_api/requiremets.txt
RUN yum install gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel wget make -y
WORKDIR /usr/src
RUN wget https://www.python.org/ftp/python/3.7.11/Python-3.7.11.tgz
RUN tar xzf Python-3.7.11.tgz
WORKDIR /usr/src/Python-3.7.11
RUN ./configure --enable-optimizations
RUN  make altinstall
# RUN yum install python3 python3-pip -y
RUN pip3.7 install -r /python_api/requiremets.txt
CMD ["python3.7", "/python_api/python-api.py"]
